<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends CI_Controller
{
    public function __construct()
    {

        parent::__construct();
        check_login();
    }

    public function new_page()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['title'] = 'New Page';

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('page/new_page', $data);
        $this->load->view('templates/footer');
    }

    public function add_pageContent()
    {
        $this->Page_model->add_contetnt();
    }
}
