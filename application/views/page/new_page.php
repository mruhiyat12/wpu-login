<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="row">
        <div class="col-lg">
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>

            <?= $this->session->flashdata('message'); ?>

            <form action="<?= base_url('page/add_pageContent'); ?>" method="POST">
                <div class="mb-3 col-md ">
                    <div class="row mb-2">
                        <div class="col-md">
                            <h5 class="">Title <p style="font-size: 12px;">Insert Text for Title</p>
                            </h5>
                            <textarea name="title" id="title" style="min-width: 50%; min-height: 100px;"></textarea>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h5 class="">Page Content
                            </h5>
                            <textarea class="ckeditor " name="content" id="content"></textarea>
                        </div>
                    </div>

                </div>

                <div class="">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-fw fa-paper-plane"></i> Send</button>
                </div>
            </form>

        </div>
    </div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->