<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page_model extends CI_Model
{

    public function __construct()
    {

        parent::__construct();
        $this->db2 = $this->load->database('new_web', TRUE);
    }

    public function add_contetnt()
    {
        $data = [
            'title' => $this->input->post('title'),
            'content' => $this->input->post('content'),
            'date_created' => time()
        ];

        $query = $this->db2->insert('page_content', $data);

        if ($query) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Page content has been add!</div>');
            redirect('page/new_page');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
         Page content failed to add!</div>');
            redirect('page/new_page');
        }
    }
}
